#ifndef __Connection_h_
#define __Connection_h_

class NavigationNode;

class Connection
{
public:
	Connection()
	{}

	Connection(NavigationNode* _fromNode, NavigationNode* _toNode, float _cost):
		fromNode(_fromNode),
		toNode(_toNode),
		cost(_cost)
	{}

	NavigationNode* getFromNode()
	{
		return fromNode;
	}

	const NavigationNode* getFromNode() const
	{
		return fromNode;
	}

	NavigationNode* getToNode()
	{
		return toNode;
	}

	const NavigationNode* getToNode() const
	{
		return toNode;
	}

	float getCost() const
	{
		return cost;
	}

private:
	NavigationNode* fromNode;
	NavigationNode* toNode;
	float cost;
};

#endif