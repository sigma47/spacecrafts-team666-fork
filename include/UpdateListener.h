#pragma once

#include "../External/filewatcher/include/FileWatcher.h"
#include "../External/filewatcher/include/FileWatcherWin32.h"
#include <iostream>
#include <string>
#include "LuaScriptManager.h"

class UpdateListener : public FW::FileWatchListener
{
public:
	UpdateListener();
	void Init(scripting::Manager* m);
	void UpdateListener::handleFileAction(FW::WatchID watchid, const std::string& dir, const std::string& filename, FW::Action action);
private:
	scripting::Manager* mScriptM;
};