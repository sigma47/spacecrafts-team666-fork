 #ifndef __AIController_h_
 #define __AIController_h_ 

#include "Decision.h"
#include "SpacecraftController.h"
#include "Path.h"
#include "luabind/object.hpp"

class AIController: public SpacecraftController
{
	typedef std::vector<Action*> ActionVector;
	typedef std::vector<Decision*> DecisionVector;
public:
	AIController(Spacecraft* spacecraft, Spacecraft* mHumanSpacecraft, const Path& patrol);
	~AIController(void);

	virtual void update(float delta);

	// returns the patrol path of this player
	const Path& getPatrolPath() const
	{
		return mPath;
	}

	// returns the human craft
	Spacecraft* getHumanSpacecraft()
	{
		return mHumanSpacecraft;
	}

	/// returns the start position of this ai player
	Ogre::Vector3 getStartPosition()
	{
		return mPath.getPosition(0);
	}

	Ogre::Vector3 getPatrol()
	{
		return followPath(mPath, mCurrentParam);
	}

private:
	Spacecraft* mHumanSpacecraft;

	Path mPath;
	float mCurrentParam;

	luabind::object root;

	ActionVector m_vActions;
	DecisionVector m_vDecisions;
	BaseNode* m_pRoot;
};


#endif