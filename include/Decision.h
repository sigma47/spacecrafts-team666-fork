#pragma once

class Action;
class BaseNode
{
public:
	virtual Action& decide(void) = 0;
};

class Action :
	public BaseNode
{
public:
	Action& decide(void) override final;

	virtual void execute(double dt) = 0;
};

class Decision :
	public BaseNode
{
public:
	Decision(BaseNode& yes, BaseNode& no);

	Action& decide(void) override final;

private:

	virtual bool condition(void) = 0;

	BaseNode* m_pYes, *m_pNo;
};

