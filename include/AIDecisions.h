#pragma once
#include "Decision.h"

class Spacecraft;
class AIController;

/***************************************
* Patrol
****************************************/

class PatrolAction : 
	public Action
{
public:

	PatrolAction(AIController& controller);

	void execute(double dt) override;

private:

	AIController* m_pController;
};

/***************************************
* Pursue
****************************************/

class PursueAction :
	public Action
{
public:

	PursueAction(AIController& controller);

	void execute(double dt) override;

private:

	AIController* m_pController;
	float m_currentParam;
};

/***************************************
* Flee
****************************************/

class FleeAction :
	public Action
{
public:

	void execute(double dt) override;
};

/***************************************
* Fire
****************************************/

class FireAction :
	public Action
{
public:

	FireAction(AIController& controller);

	void execute(double dt) override;

private:
	
	AIController* m_pController;

};

/***************************************
* EnemyNear
****************************************/

class EnemyNearDecision :
	public Decision
{
public:

	EnemyNearDecision(BaseNode& yes, BaseNode& no, Spacecraft& spacecraft, Spacecraft& enemy, float range);

	bool condition(void) override;

private:

	float m_range;

	Spacecraft* m_pSpacecraft, *m_pEnemy;
};

/***************************************
* IsHealthy
****************************************/

class IsHealthyDecision :
	public Decision
{
public:

	IsHealthyDecision(BaseNode& yes, BaseNode& no, Spacecraft& spacecraft);

	bool condition(void) override;

private:

	Spacecraft* m_pSpacecraft;
};