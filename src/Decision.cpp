#include "Decision.h"

Action& Action::decide(void)
{
	return *this;
}

Decision::Decision(BaseNode& yes, BaseNode& no) : m_pYes(&yes), m_pNo(&no)
{
}

Action& Decision::decide(void)
{
	if(condition())
		return m_pYes->decide();
	else
		return m_pNo->decide();
}
