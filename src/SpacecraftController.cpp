#include "StdAfx.h"
#include "GameApplication.h"
#include "SpacecraftController.h"
#include "DebugDisplay.h"
#include "GameConfig.h"
#include "Path.h"
#include "MathUtil.h"
#include "WorldUtil.h"

using namespace Ogre;

static ColourValue DEBUG_COLOUR_WALL_AVOIDANCE(1.0f, 0.0f, 0.0f);
static ColourValue DEBUG_COLOUR_OBSTALCE_AVOIDANCE(1.0f, 0.0f, 1.0f);


SpacecraftController::SpacecraftController(Spacecraft* spacecraft):
	mSpacecraft(spacecraft)
{}


SpacecraftController::~SpacecraftController()
{}

Ogre::Vector3 SpacecraftController::seek(const Ogre::Vector3& target) const
{
	Ogre::Vector3 desiredVelocity = target - mSpacecraft->getPosition();
	desiredVelocity.normalise();
	desiredVelocity *= Spacecraft::MAX_SPEED;

	return desiredVelocity - mSpacecraft->getLinearVelocity();
}

Ogre::Vector3 SpacecraftController::arrive(const Ogre::Vector3& target) const
{
	DebugDisplay::getSingleton().drawCircle(target, 1.0f, 16, ColourValue::Red);

	Ogre::Vector3 toTarget = target - mSpacecraft->getPosition();
	float dist = toTarget.length();

	if (dist > 0)
	{
		const float DECELERATION_TWEAKER = GameConfig::getSingleton().getValueAsReal("Steering/DecelerationTweaker");
		float speed = dist / DECELERATION_TWEAKER;

		speed = std::min(speed, Spacecraft::MAX_SPEED);
		Ogre::Vector3 desiredVelocity = toTarget * speed / dist;

		return desiredVelocity - mSpacecraft->getLinearVelocity();
	}

	return Ogre::Vector3(0.0f);
}

Ogre::Vector3 SpacecraftController::followPath(const Path& path, float& currentParam) const
{
	const float TARGET_OFFSET = 50.f;
	
	currentParam = path.getParam(mSpacecraft->getPosition(), currentParam);
	DebugDisplay::getSingleton().drawCircle(path.getPosition(currentParam), 1.0f, 16, ColourValue::Green);
	float targetParam = currentParam + TARGET_OFFSET;
	Ogre::Vector3 target = path.getPosition(targetParam);

	DebugDisplay::getSingleton().drawCircle(target, 1.0f, 16, ColourValue::Red);
	return	seek(target);
}


Ogre::Vector3 SpacecraftController::wallAvoidance() const
{
	const float LOOK_AHEAD_TIME = 10.0f;
	const float AVOID_DISTANCE = 2.f * mSpacecraft->getRadius();
	
	Ogre::Vector3 start = mSpacecraft->getPosition();
	Ogre::Vector3 end = mSpacecraft->getPosition() + mSpacecraft->getLinearVelocity() * LOOK_AHEAD_TIME;
	Ogre::Vector3 colPoint;
	Ogre::Vector3 colNormal;

	if (WorldUtil::rayCast(start, end, colPoint, colNormal)) 
	{
		Ogre::Vector3 target = colPoint + colNormal	* AVOID_DISTANCE;
		return	seek(target);
	}
	
	return	Ogre::Vector3(0, 0, 0);

}


Ogre::Vector3 SpacecraftController::obstacleAvoidance() const
{
	Ogre::Vector3 steering(0, 0, 0);
	const float LOOK_AHEAD_TIME = 1.0f;
	const std::vector<Spacecraft*> targets = GameApplication::getSingletonPtr()->getSpacecrafts();

	for (auto& target : targets)
	{
		if (target->getId() != mSpacecraft->getId())
		{
			float t1, t2;
			if (MathUtil::sphereSweepTest(mSpacecraft->getRadius(), mSpacecraft->getPosition(), mSpacecraft->getLinearVelocity(),
				target->getRadius(), target->getPosition(),
				target->getLinearVelocity(), t1, t2))
			{
				if (t1 > 0 && t1 < LOOK_AHEAD_TIME)
				{
					Ogre::Vector3	targetColPos = target->getPosition() * target->getLinearVelocity() * t1;
					Ogre::Vector3	collisionPos = mSpacecraft->getPosition() * mSpacecraft->getLinearVelocity() * t1;
					Ogre::Vector3	direction = targetColPos - collisionPos;
					direction.normalise();

					steering += -direction * Spacecraft::MAX_SPEED;
				}
			}
		}
	}
	
	return	steering;

}