#include "stdafx.h"

#include <stack>
#include <list>
#include "NavigationGraph.h"
#include "NavigationNode.h"
#include "Connection.h"
#include "PathfindingList.h"
#include "NavigationGraphDebugDisplay.h"
#include "DebugDisplay.h"
#include "WorldUtil.h"

using namespace Ogre;
using namespace OgreBulletDynamics;
using namespace OgreBulletCollisions;

template<> NavigationGraph* Ogre::Singleton<NavigationGraph>::msSingleton = 0;


NavigationGraph::NavigationGraph(SceneManager* sceneMgr, int _x, int _z, int _width, int _height):
	origin((float) _x, 0.0f, (float) _z),
	width(_width),
	height(_height)
{
	mDebugDisplay = new NavigationGraphDebugDisplay(sceneMgr, 0.5f);
	gridWidth = (int) floor(width / NavigationNode::NODE_SIZE);
	gridDepth = (int) floor(height / NavigationNode::NODE_SIZE);
	
	int gridSize = gridWidth * gridDepth;
	grid.reserve(gridSize);
	
	for (int i = 0; i < gridSize; i++)
	{
		grid.push_back(NULL);
	}
}


NavigationGraph::~NavigationGraph()
{
	for (size_t i = 0; i < grid.size(); i++)
	{
		delete grid[i];
	}

	grid.clear();
}

NavigationGraph* NavigationGraph::getSingletonPtr(void)
{
    return msSingleton;
}
NavigationGraph& NavigationGraph::getSingleton(void)
{  
    assert( msSingleton );  return ( *msSingleton );  
}

void NavigationGraph::setDebugDisplayEnabled(bool enable)
{
	mDebugDisplay->setEnabled(enable);
}

bool NavigationGraph::checkSpaceForNode(OgreBulletDynamics::DynamicsWorld* world, const Vector3& position) const
{
	const Vector3 offset[] = { 
		Vector3(-NavigationNode::NODE_SIZE_HALF*0.5f, 0.0f, 0.0f), 
		Vector3(NavigationNode::NODE_SIZE_HALF*0.5f, 0.0f, 0.0f), 
		Vector3(0.0f, 0.0f, -NavigationNode::NODE_SIZE_HALF*0.5f), 
		Vector3(0.0f, 0.0f, NavigationNode::NODE_SIZE_HALF*0.5f) 
	};

	int blocked = 0;

	// send multiple rays to check if spot is free.
	for (int i = 0; i < 4; i++)
	{
		btVector3 start(position.x + offset[i].x, 100.0f, position.z + offset[i].z);
		btVector3 end(position.x + offset[i].x, 0.0f, position.z + offset[i].z);
			
		btCollisionWorld::ClosestRayResultCallback rayCallback(start, end);
 
		// Perform raycast
		world->getBulletCollisionWorld()->rayTest(start, end, rayCallback);
 
		if(rayCallback.hasHit()) 
		{
			return false;
		}
	}

	return true;
}

bool NavigationGraph::rayTest(OgreBulletDynamics::DynamicsWorld* world, const Vector3& start, const Vector3& end) const
{
	btVector3 startBt = OgreBtConverter::to(start);
	btVector3 endBt = OgreBtConverter::to(end);

	btCollisionWorld::ClosestRayResultCallback rayCallback(startBt, endBt);
 
	// Perform raycast
	world->getBulletCollisionWorld()->rayTest(startBt, endBt, rayCallback);
 
	return rayCallback.hasHit();
}


void NavigationGraph::calcGraph(OgreBulletDynamics::DynamicsWorld* world)
{
	for (int z = 0; z < gridDepth; z++)
	{
		for (int x = 0; x < gridWidth; x++)
		{
			Vector3 pos((float) (x * NavigationNode::NODE_SIZE), 5.0f,
					 (float) (z * NavigationNode::NODE_SIZE));

			pos += origin;

			if(!checkSpaceForNode(world, pos))
			{
				continue;
			}
			
			NavigationNode* node = new NavigationNode(Vector3((float) x, 0.0f, (float) z), pos);
			NavigationNode* leftNode = getNode(x-1, z);
			NavigationNode* topNode = getNode(x, z-1);
			
			if (leftNode != NULL && !rayTest(world, leftNode->center, node->center))
			{
				Vector3 distance = node->center - leftNode->center;
				float cost = distance.length();
				node->addConnection(Connection(node, leftNode, cost));
				leftNode->addConnection(Connection(leftNode, node, cost));
			}
			if (topNode != NULL && !rayTest(world, topNode->center, node->center))
			{
				Vector3 distance = node->center - topNode->center;
				float cost = distance.length();
				node->addConnection(Connection(node, topNode, cost));
				topNode->addConnection(Connection(topNode, node, cost));
			}

			grid[x + z * gridWidth] = node;
		}
	}

	debugDraw();
}

template<class T>
static T round(T r) 
{
	return (r > (T) 0.0) ? floor(r + (T) 0.5) : ceil(r - (T) 0.5);
}

NavigationNode* NavigationGraph::getNodeAt(const Vector3& position)
{
	int idxX = (int) round((position.x - origin.x) / NavigationNode::NODE_SIZE);
	int idxZ = (int) round((position.z - origin.z) / NavigationNode::NODE_SIZE);
	
	return getNode(idxX, idxZ);
}

NavigationNode* NavigationGraph::getNearestNode(const Vector3& position)
{
	int idxX = (int)round((position.x - origin.x) / NavigationNode::NODE_SIZE);
	int idxZ = (int)round((position.z - origin.z) / NavigationNode::NODE_SIZE);

	Vector2 start(idxX, idxZ);

	static const int MAX_DEPTH = 100;
	static const int OFFSET_LENGHT = 4;
	static const Vector2 OFFSET[OFFSET_LENGHT] = { Vector2(0, 1), Vector2(1, 0), Vector2(0, -1), Vector2(-1, 0) };

	std::deque<Vector2> open;
	std::deque<Vector2> closed;

	open.push_back(start);

	int depth = 0;

	while (open.size() && (depth++ < MAX_DEPTH))
	{
		Vector2 current = open.front();
		open.pop_front();

		NavigationNode* node = getNode(current.x, current.y);

		Vector3 collPoint, collNormal;

		if (node != NULL && !WorldUtil::rayCast(position, node->getCenter(), collPoint, collNormal))
		{
			return node;
		}

		for (int i = 0; i < OFFSET_LENGHT; i++)
		{
			Vector2 neighbor = current + OFFSET[i];

			if ((std::find(open.begin(), open.end(), neighbor) == open.end()) &&
				(std::find(closed.begin(), closed.end(), neighbor) == closed.end()))
			{
				open.push_back(neighbor);
			}
		}

		closed.push_back(current);
		depth++;
	}

	return NULL;
}

NavigationNode* NavigationGraph::getNode(int idxX, int idxZ)
{
	if ((idxX >= 0) && (idxX < gridWidth) && (idxZ >= 0) && (idxZ < gridDepth))
	{
		return grid[idxX + idxZ * gridWidth];
	}
	else
	{
		return NULL;
	}
}

void NavigationGraph::debugDraw() const
{
	for (size_t i = 0; i < grid.size(); i++)
	{
		if (grid[i] != NULL)
		{
			grid[i]->debugDraw(mDebugDisplay);
		}
	}

	mDebugDisplay->build();
}

// node - cost
typedef std::list<NavigationNode*> NodeList;

std::vector<Vector3> NavigationGraph::calcPath(const Vector3& currentPosition, const Vector3& targetPosition)
{
	NavigationNode* start = getNodeAt(currentPosition);
	NavigationNode* goal = getNodeAt(targetPosition);

	std::vector<Vector3> path;

	// in case the start position is not on the grid, start a search for the nearest node
	// that we can start from
	// this is basically a hack for the fact the spaceships can slide off the grid e.g. on the corners
	// of a wall
	if(!start && goal)
	{
		static const int searchDist = 5;

		for(int x = -searchDist; x <= searchDist; x++)
		{
			for(int z = -searchDist; z <= searchDist; z++)
			{
				auto newStart = getNodeAt(currentPosition + Vector3((float)x, 0.0f, (float)z));

				if(newStart && newStart != start)
				{
					const auto distance = goal->center - newStart->center;
					newStart->cost = distance.length();

					if(!start || start->cost > newStart->cost)
						start = newStart;
				}
			}
		}
	}

	if (start == NULL || goal == NULL)
	{
		return path;
	}

	// implement a A*

	NodeList openList;
	NodeList closeList;

	// initialization - calculate cost from start to end node
	const auto distance = goal->center - start->center;
	start->cost = distance.length();

	openList.emplace_back(start);
	start->opened = true;

	NavigationNode* current = nullptr;

	while(!current)
	{
		for(auto pNode : openList)
		{
			// determine node with smallest estimated cost
			if(!current || pNode->cost < current->cost)
			{
				current = pNode;
			}
		}

		if(current == goal)
			break;

		openList.remove(current);
		current->opened = false;

		closeList.push_back(current);
		current->closed = true;

		for(auto& connection : current->getConnections())
		{
			auto pTargetNode = connection.getToNode();

			if(pTargetNode->closed)
				continue;

			// set the cost that we know for sure incures by traveling, so we can iteratively calculate this
			pTargetNode->knownCost = connection.getCost() + current->knownCost;

			// calculate cost from start to end node over this node
			const auto distance = goal->center - pTargetNode->center;
			pTargetNode->cost = pTargetNode->knownCost + distance.length();

			if(!pTargetNode->opened)
			{
				pTargetNode->pParent = current;

				openList.push_back(pTargetNode);
				pTargetNode->opened = true;
			}
		}

		current = nullptr;
	}

	// reset opened/closed flags
	for(auto pNode : openList)
	{
		pNode->opened = false;
	}

	for(auto pNode : closeList)
	{
		pNode->closed = false;
	}

	// create path
	while(current->pParent && current != start)
	{
		path.push_back(current->center);
		current = current->pParent;
	}

	const unsigned test = path.size();

	return path;

}
