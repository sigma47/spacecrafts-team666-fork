#include "UpdateListener.h"
#include <Windows.h>

UpdateListener::UpdateListener()
{};

void UpdateListener::Init(scripting::Manager* m)
{
	mScriptM = m;
}

void UpdateListener::handleFileAction(FW::WatchID watchid, const std::string& dir, const std::string& filename,
	FW::Action action)
{
	switch (action)
	{
	case FW::Actions::Add:
		//std::cout << "File (" << dir + "\\" + filename << ") Added! " << std::endl;
		break;
	case FW::Actions::Delete:
		//std::cout << "File (" << dir + "\\" + filename << ") Deleted! " << std::endl;
		break;
	case FW::Actions::Modified:
		if (filename == "testscript.lua")
			mScriptM->runScriptFile("../../media/testscript.lua");
		break;
	default:
		std::cout << "Should never happen!" << std::endl;
	}
}