#include "..\include\AIDecisions.h"
#include "AIController.h"
#include "Spacecraft.h"
#include "NavigationGraph.h"

/***************************************
* Patrol
****************************************/

PatrolAction::PatrolAction(AIController& controller) : 
	m_pController(&controller)
{
}

void PatrolAction::execute(double dt)
{
	const auto linearSteering = m_pController->getPatrol() + m_pController->obstacleAvoidance() + m_pController->wallAvoidance();

	m_pController->getSpacecraft()->setSteeringCommand(linearSteering);
}

/***************************************
* Pursue
****************************************/

PursueAction::PursueAction(AIController& controller) :
	m_pController(&controller), m_currentParam(0.0f)
{
}

void PursueAction::execute(double dt)
{
	const auto vPath = NavigationGraph::getSingletonPtr()->calcPath(m_pController->getSpacecraft()->getPosition(), m_pController->getHumanSpacecraft()->getPosition());
	
	auto linearSteering = m_pController->obstacleAvoidance() + m_pController->wallAvoidance();

	if(vPath.size() > 1)
	{
		const Path path(vPath, Path::PathType::PATH_NORMAL);

		linearSteering += m_pController->followPath(path, m_currentParam);
	}

	m_pController->getSpacecraft()->setSteeringCommand(linearSteering);
}

/***************************************
* Fire
****************************************/

FireAction::FireAction(AIController& controller) :
	m_pController(&controller)
{
}

void FireAction::execute(double dt)
{
	auto pSpacecraft = m_pController->getSpacecraft();

	pSpacecraft->shoot();
}

/***************************************
* EnemyNear
****************************************/

EnemyNearDecision::EnemyNearDecision(BaseNode& yes, BaseNode& no, Spacecraft& spacecraft, Spacecraft& enemy, float range) :
	Decision(yes, no), m_range(range), m_pSpacecraft(&spacecraft), m_pEnemy(&enemy)
{
}

bool EnemyNearDecision::condition(void)
{
	const auto distance = (m_pEnemy->getPosition() - m_pSpacecraft->getPosition()).length();
	return distance < m_range;
}

/***************************************
* IsHealthy
****************************************/

IsHealthyDecision::IsHealthyDecision(BaseNode& yes, BaseNode& no, Spacecraft& spacecraft) :
	Decision(yes, no), m_pSpacecraft(&spacecraft)
{
}

bool IsHealthyDecision::condition(void)
{
	return m_pSpacecraft->getHealth() > 0.2f;
}