#include "StdAfx.h"

#include <algorithm> 
#include "AIController.h"
#include "DebugOverlay.h"

#include "DebugDisplay.h"

#include "NavigationGraph.h"
#include "GameConfig.h"
#include "GameApplication.h"
#include "MathUtil.h"
#include "LuaBind/object.hpp"
#include "LuaScriptManager.h"

#include "AIDecisions.h"

using namespace Ogre;


AIController::AIController(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, const Path& patrol):
	SpacecraftController(spacecraft),
	mHumanSpacecraft(humanSpacecraft),
	mCurrentParam(0.0f),
	mPath(patrol)
{
	root = scripting::Manager::getSingleton().callFunction<luabind::object>("init", this);

	// create a overlay to render the behavior tree
	DebugOverlay::getSingleton().addTextBox("ai" + spacecraft->getId(), "", 0, 100 + (150 * (spacecraft->getId()-1)), 300, 150);

	// setup actions & decisions

	auto patrolAction = new PatrolAction(*this);
	auto pursueAction = new PursueAction(*this);
	auto fireAction = new FireAction(*this);

	m_vActions.push_back(patrolAction);
	m_vActions.push_back(pursueAction);
	m_vActions.push_back(fireAction);

	auto inFireRangeDecision = new EnemyNearDecision(*fireAction, *pursueAction, *mSpacecraft, *mHumanSpacecraft, 30.0f);
	auto isHealthyDecision = new IsHealthyDecision(*inFireRangeDecision, *patrolAction, *mSpacecraft);
	auto inRangeDecision = new EnemyNearDecision(*isHealthyDecision, *patrolAction, *mSpacecraft, *mHumanSpacecraft, 90.0f);

	m_vDecisions.push_back(inFireRangeDecision);
	m_vDecisions.push_back(isHealthyDecision);
	m_vDecisions.push_back(inRangeDecision);

	m_pRoot = inRangeDecision;
}

AIController::~AIController(void)
{
	for(auto pAction : m_vActions)
	{
		delete pAction;
	}

	for(auto pDecision : m_vDecisions)
	{
		delete pDecision;
	}
}

void AIController::update(float delta)
{
	if (root)
	{
		root["tick"](root, delta);

		// "renders" the behavior tree on the overlay
		std::string text = luabind::object_cast<std::string>(root["printTree"](root, ""));
		DebugOverlay::getSingleton().setText("ai" + getSpacecraft()->getId(), text);
	}

	auto& action = m_pRoot->decide();
	action.execute(delta);
}


 
