#include "stdafx.h"
#include "GameExports.h"

#include "Spacecraft.h"
#include "SpacecraftController.h"
#include "AIController.h"
#include "WorldUtil.h"
#include "NavigationGraph.h"

#include "luabind/luabind.hpp"
#include "luabind/operator.hpp"
#include "luabind/wrapper_base.hpp"
#include "luabind/class.hpp"
#include "luabind/adopt_policy.hpp"


namespace scripting
{
	static void log(Ogre::String text)
	{
		Ogre::LogManager::getSingletonPtr()->logMessage(text);
	}

	class AIControllerWrapper : public AIController, public luabind::wrap_base
	{
	public:
		AIControllerWrapper(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, const Path& patrol)
			: AIController(spacecraft, humanSpacecraft, patrol)
		{
		}

		virtual ~AIControllerWrapper()
		{
		}
		
		virtual void update(float delta)
		{
			luabind::call_member<void>(this, "update", delta);
		}

		static void default_update(AIController* ptr, float delta)
		{
			ptr->AIController::update(delta);
		}
	};

	void GameExports::extendState(lua_State* pState)
	{
		luabind::module(pState)
				[
					luabind::class_<Spacecraft>("Spacecraft")
					.def("getPosition", &Spacecraft::getPosition)
					.def("getRadius", &Spacecraft::getRadius)
					.def("getHealth", &Spacecraft::getHealth)
					.def("getSceneNode", &Spacecraft::getSceneNode)
					.def("getDirection", &Spacecraft::getDirection)
					.def("getOrientation", &Spacecraft::getOrientation)
					.def("setOrientation", &Spacecraft::setOrientation)
					.def("getYaw", &Spacecraft::getYaw)
					.def("getId", &Spacecraft::getId)
					.def("getLinearVelocity", &Spacecraft::getLinearVelocity)
					.def("getAngularVelocity", &Spacecraft::getAngularVelocity)
					.def("setSteeringCommand", (void (Spacecraft::*)(const Ogre::Vector3&, float))&Spacecraft::setSteeringCommand)
					.def("setSteeringCommand", (void (Spacecraft::*)(const Ogre::Vector3&))&Spacecraft::setSteeringCommand)
					.def("update", &Spacecraft::update)
					.def("onCollision", &Spacecraft::onCollision)
					.def("shoot", &Spacecraft::shoot)
					.def("hit", &Spacecraft::hit), 

					
					luabind::class_<Path>("Path")
					.def("getParam", &Path::getParam)
					.def("getPosition", &Path::getPosition)
					.def("getTotalLength", &Path::getTotalLength)
					.def("isPathEnd", &Path::isPathEnd)
					.def("isEmpty", &Path::isEmpty)
					.def("debugDraw", &Path::debugDraw),

					luabind::class_<SpacecraftController>("SpacecraftController")
					.def("seek", &SpacecraftController::seek)
					.def("arrive", &SpacecraftController::arrive)
					.def("update", &SpacecraftController::update)
					.def("getSpacecraft", &SpacecraftController::getSpacecraft)
					.def("followPath", &SpacecraftController::followPathLua)
					.def("obstacleAvoidance", &SpacecraftController::obstacleAvoidance),

					luabind::class_<AIController, AIControllerWrapper, luabind::bases<SpacecraftController> >("AIController")
					.def(luabind::constructor<Spacecraft*, Spacecraft*, const Path&>())
					.def("update", &AIController::update, &AIControllerWrapper::default_update)
					.def("getPatrolPath", &AIController::getPatrolPath)
					.def("getHumanSpacecraft", &AIController::getHumanSpacecraft)
					.def("getStartPosition", &AIController::getStartPosition),

					luabind::class_<ICollider>("ICollider"),

					luabind::class_<NavigationGraph>("NavigationGraph")
					.def("getPath", &NavigationGraph::getPath)
					.scope
					[
						luabind::def("getSingleton", &NavigationGraph::getSingleton)
					],

					luabind::class_<WorldUtil>("WorldUtil")
					.scope
					[
						luabind::def("rayCast", (bool(*)(Spacecraft*, float lookAheadTime, Ogre::Vector3&, Ogre::Vector3&)) &WorldUtil::rayCast),
						luabind::def("rayCast", (bool(*)(const Ogre::Vector3&, const Ogre::Vector3&, Ogre::Vector3&, Ogre::Vector3&)) &WorldUtil::rayCast)
					],

					luabind::def("log", &log)
				]; 
	}
}