--------------------------------
-- BEHAVIOR TREE BASE CLASSES --
--------------------------------

-- Return values of a Behavior
STATUS_INVALID = 0;
STATUS_RUNNING = 1;
STATUS_SUCCESS = 2;
STATUS_FAILURE = 3;
STATUS_NAMES = {[STATUS_INVALID] = "STATUS_INVALID", [STATUS_RUNNING] = "STATUS_RUNNING", [STATUS_SUCCESS] = "STATUS_SUCCESS", [STATUS_FAILURE] = "STATUS_FAILURE"}

----------------------------------------------
--                 BEHAVIOR 
--         Base calss for all behaviors
----------------------------------------------

class 'Behavior'

function Behavior:__init()
	self.name = "Behavior"
	self.status = STATUS_INVALID
end

-- Callback called when behavior gets active
function Behavior:onInitialized()
end

-- Callback called every tick while the behavior is active
-- Note: MUST return a status.
function Behavior:onUpdate(delta)
	return STATUS_SUCCESS
end

-- Callback when the behavior is terminated
function Behavior:onTerminated()
end

-- returns a debug string ot the tree
function Behavior:printTree(space)
	return space .. self.name .. " status: " .. STATUS_NAMES[self.status]
end

-- resets the Behavior to invalid
function Behavior:reset()
	self.status = STATUS_INVALID
end

-- updates the behavior and calls the callbacks
-- NOTE: do not override! use the callbacks instead
function Behavior:tick(delta)
	if self.status == STATUS_INVALID then
		self:onInitialized()
	end

	self.status = self:onUpdate(delta)
	
	if self.status ~= STATUS_RUNNING then
		self:onTerminated()
	end
	
	return self.status
end

----------------------------------------------
--               COMPOSITE 
-- Base calss for all behaviors with children
----------------------------------------------


class 'Composite' (Behavior)

function Composite:__init() super()
	self.name = "Composite"
	self.children = {}
end

-- adds a new child to the composite
function Composite:add(child)
	table.insert(self.children, child);
end

function Composite:printTree(space)
	local result = Behavior.printTree(self, space)
	
	for k,v in pairs(self.children) do
		result = result .. "\n" .. v:printTree(space .. " ")
	end

	return result
end

function Composite:reset()
	Behavior.reset(self)

	for k,v in pairs(self.children) do
		v:reset()
	end
end

----------------------------------------------
--                SEQUENCE 
--     Executes all children as sequence.
--     Failes as soon as one child failes.
----------------------------------------------

class 'Sequence' (Composite)

function Sequence:__init() super()
	self.name = "Sequence"
end

function Sequence:onInitialized()
	self.currentChild = 1
end

function Sequence:onUpdate(delta)
	if  #self.children == 0 then
		return STATUS_FAILURE
	end

	for i = self.currentChild, #self.children do
		local child = self.children[i];
		
		local s = child:tick(delta);

		if s == nil then
			s = STATUS_INVALID
		end
		
		if s ~= STATUS_SUCCESS then
			return s
		end

		self.currentChild = self.currentChild + 1
		
		if self.currentChild > #self.children then
			self.currentChild = #self.children
			return STATUS_SUCCESS
		end
	end
	
	return STATUS_INVALID
end

----------------------------------------------
--                SELECTOR 
-- Tries all children until 1 child succeeds.
----------------------------------------------

class 'Selector' (Composite)

function Selector:__init() super()
	self.name = "Selector"
end

function Selector:onInitialized()
	self.currentChild = 1
end

function Selector:onUpdate(delta)
	if  #self.children == 0 then
		return STATUS_FAILURE
	end

	for i = self.currentChild, #self.children do
		local child = self.children[i];
		
		local s = child:tick(delta);
		
		if s ~= STATUS_FAILURE then
			return s
		end

		self.currentChild = self.currentChild + 1
		
		if self.currentChild > #self.children then
			self.currentChild = #self.children
			return STATUS_FAILURE
		end
	end
	
	return STATUS_INVALID
end


----------------------------------------------
--             PRIORITY SELECTOR 
--  Restarts the chain every tick and selects
--   the first behavior which doesn't fail.
----------------------------------------------

class 'PrioritySelector' (Composite)

function PrioritySelector:__init() super()
	self.name = "PrioritySelector"
	self.lastChild = 1
end

function PrioritySelector:onUpdate(delta)
	if  #self.children == 0 then
		return STATUS_FAILURE
	end

	for i = 1, #self.children do
		local child = self.children[i];

		local s = child:tick(delta)
		
		if s ~= STATUS_FAILURE then
			-- reset the following children if they were active the last time.
			for j = i + 1, self.lastChild do
				self.children[j]:reset()
			end

			self.lastChild = i
			return s
		end
	
		if i > #self.children then
			self.lastChild = i
			return STATUS_FAILURE
		end
	end
	
	return STATUS_INVALID
end

----------------------------------------------
--                  PARALLEL 
--  Runs all children at the same time until
--  1 child failes.
----------------------------------------------
class 'Parallel' (Composite)

function Parallel:__init() super()
	self.name = 'Parallel'
end

function Parallel:onUpdate(delta)
	if  #self.children == 0 then
		return STATUS_FAILURE
	end

	local result = STATUS_SUCCESS

	for i = 1, #self.children do
		local child = self.children[i];
		
		local s = child:tick(delta);
		
		if s == STATUS_FAILURE then
			return s
		end

		if s == STATUS_RUNNING then
			result = STATUS_RUNNING
		end 
	end
	
	return result
end

----------------------------------------------
--                  ACTION 
--          Base class for actions
----------------------------------------------
class 'Action' (Behavior)

function Action:__init(controller) super()
	self.controller = controller
	self.name = "Action"
	
end

function Action:onUpdate(delta)
	return STATUS_RUNNING
end

----------------------------------------------
--                  CONDITION 
--          Base class for actions
-- Note: override testCondition
----------------------------------------------
class 'Condition' (Behavior)

function Condition:__init() super()
	self.name = "Condition"
end

-- 
function Condition:testCondition()
	return false
end
	
function Condition:onUpdate()
	if self:testCondition() then
		return STATUS_SUCCESS
	else
		return STATUS_FAILURE
	end
end


----------------------------------------------
-- Custom implementation for the game
----------------------------------------------

-- Follows the patrol path of the spacecraft
class "PatrolAction" (Action)

function PatrolAction:__init(controller) super(controller)
	self.name = "PatrolAction"
end

function PatrolAction:onUpdate(delta)
	local path = self.controller:getPatrolPath()
	local linear = self.controller:followPath(path) + self.controller:obstacleAvoidance()
	self.controller:getSpacecraft():setSteeringCommand(linear)
	return STATUS_RUNNING
end

-- follows the player
class "AttackAction" (Action)

function AttackAction:__init(controller) super(controller)
	self.name = "AttackAction"
end

function AttackAction:onUpdate(delta)
	local enemyShip = self.controller:getHumanSpacecraft()
	local linear = self.controller:seek(enemyShip:getPosition())
	self.controller:getSpacecraft():setSteeringCommand(linear)
	return STATUS_RUNNING
end

-- checks if enemy is near
class "EnemyNearCondition" (Condition)

function EnemyNearCondition:__init(controller, distance) super()
	self.name = "EnemyNearCondition"
	self.controller = controller
	self.distance = distance
end

function EnemyNearCondition:testCondition()
	local myShip = self.controller:getSpacecraft();
	local enemyShip = self.controller:getHumanSpacecraft();

	return myShip:getPosition():distance(enemyShip:getPosition()) < self.distance
end

-- returns to the start position of the spacecraft
class "GotoBaseAction" (Action)

function GotoBaseAction:__init(controller) super(controller)
	self.name = "GotoBaseAction"
end

function GotoBaseAction:onInitialized()
	local graph = NavigationGraph.getSingleton()
	self.target = self.controller:getStartPosition()
	self.path = graph:getPath(self.controller:getSpacecraft():getPosition(), self.target)
end

function GotoBaseAction:onUpdate(delta)
	self.path:debugDraw()
	local linear = self.controller:followPath(self.path) + self.controller:obstacleAvoidance()
	self.controller:getSpacecraft():setSteeringCommand(linear)

	if self.controller:getSpacecraft():getPosition():distance(self.target) < 20 then
		return STATUS_SUCCESS
	end

	return STATUS_RUNNING
end



init = function(controller)
	------------------------------------------------
	--
	--                PrioritySelector
	--                      |
	--            +---------+--------+
	--            |                  |
	--        Parallel           Sequence
	--            |                  |
	--       +----+----+         +---+----+
	--       |         |         |        |
	--   EnemyNear?  Attack   GotoBase  Patrol
	--
    -------------------------------------------------

	local root = PrioritySelector()

	-- Attack tree
	local attackTree = Parallel()
	attackTree:add(EnemyNearCondition(controller, 100))
	attackTree:add(AttackAction(controller))

	-- Patrol tree
	local patrolTree = Sequence()
	patrolTree:add(GotoBaseAction(controller));
	patrolTree:add(PatrolAction(controller));

	root:add(attackTree)
	root:add(patrolTree)

	-- return node to ai controller
	return root;
end


update = function(controller)
	-- not needed
end