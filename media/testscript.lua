function foo(i)
    local r = 2*i
    return r
end

function CamFocus(deltatime, camPos, target)
	offset = target - camPos
	offset = offset * deltatime
	camPos = camPos + offset
	camPos = target
	camPos.y = camPos.y + 250
	camPos.z = camPos.z - 150
	return camPos
end